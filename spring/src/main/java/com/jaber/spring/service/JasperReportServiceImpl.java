package com.jaber.spring.service;

import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jaber
 * @date 7/24/2022
 * @time 2:08 PM
 */
@Service
@Transactional
@RequiredArgsConstructor
public class JasperReportServiceImpl implements JasperReportService {

    //    private final TeamDesignerRepository teamDesignerRepository;
    @Override
    public byte[] generateReport() throws FileNotFoundException, JRException {
        String filePath = ResourceUtils.getFile("classpath:table_element.jrxml").getAbsolutePath();
        // Fetch Data from any repository. In my case is empty
//        List<TeamDesigner> list = teamDesignerRepository.findAll();
//        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
        JasperReport jasperReport = JasperCompileManager.compileReport(filePath);
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("companyName", "Dekko Legacy Group.");
//        parameter.put("dataSource", dataSource);
        JasperPrint print = JasperFillManager.fillReport(jasperReport, parameter, new JREmptyDataSource());
        return JasperExportManager.exportReportToPdf(print);
    }
}
