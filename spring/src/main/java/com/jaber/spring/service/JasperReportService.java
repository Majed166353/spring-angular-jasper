package com.jaber.spring.service;

import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;

/**
 * @author Jaber
 * @date 7/24/2022
 * @time 2:08 PM
 */
public interface JasperReportService {
    byte[] generateReport() throws FileNotFoundException, JRException;
}
