package com.jaber.spring.controller;

import com.jaber.spring.service.JasperReportService;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api/v1/jasper-report")
@RequiredArgsConstructor
public class JasperReportController {
    private final JasperReportService jasperReportService;

    @GetMapping("/report")
    public ResponseEntity<?> generateReport() throws JRException, FileNotFoundException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_PDF);
        httpHeaders.setContentDispositionFormData("Name", "report.pdf");
        return new ResponseEntity<>(jasperReportService.generateReport(), httpHeaders, HttpStatus.OK);
    }
}
