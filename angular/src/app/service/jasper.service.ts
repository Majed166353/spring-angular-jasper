import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class JasperService {

  baseUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  getReport() {
    return this.httpClient.get(this.baseUrl + '/jasper-report/report', {observe: 'response', responseType: 'blob'});
  }
}
