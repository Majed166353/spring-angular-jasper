import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {JasperService} from "../../service/jasper.service";

@Component({
  selector: 'app-jasper',
  templateUrl: './jasper.component.html',
  styleUrls: ['./jasper.component.css']
})
export class JasperComponent implements OnInit {

  constructor(private jasperService: JasperService) {
  }

  ngOnInit(): void {
  }

  pdfView() {
    this.jasperService.getReport()
      .subscribe({
          next: (response) => {
            let blob: Blob = response.body as Blob;
            const fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
          },
          error: (errorResponse: HttpErrorResponse) => {
            console.log('getPDF error: ', errorResponse);
          }
        }
      );
  }

  downloadPdf() {
    this.jasperService.getReport()
      .subscribe({
          next: (response) => {
            const fileName: string = response.headers.get('content-disposition')?.split(';')[1].split('=')[1];
            let blob: Blob = response.body as Blob;
            let a = document.createElement('a'); //Create hyperlink
            a.download = fileName;
            a.href = window.URL.createObjectURL(blob);
            a.target = '_blank';
            a.click();
          },
          error: (errorResponse: HttpErrorResponse) => {
            console.log('getPDF error: ', errorResponse);
          }
        }
      );
  }
}
